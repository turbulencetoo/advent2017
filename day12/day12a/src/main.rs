extern crate regex;

use regex::Regex;
use std::collections::HashSet;

fn parse_input() -> Vec<HashSet<usize>> {
    // use vec as an adjacency list where the keys are the indicies
    // 0 <-> 950, 1039
    // 1 <-> 317, 904, 923
    // 2 <-> 2
    let mut out = vec![];
    let re = Regex::new(r"^.* <-> (.*)$").unwrap();
    for line in include_str!("input.txt").lines() {
        out.push(parse_line(&re, line));
    }
    out
}

fn parse_line(re: &Regex, line: &str) -> HashSet<usize> {
    let caps = re.captures(line).unwrap();
    caps.get(1)
        .unwrap()
        .as_str()
        .split(',')
        .map(|s| s.trim()
                  .parse::<usize>())
        .filter_map(|x| x.ok())
        .collect::<HashSet<_>>()
}

fn find_all_rec(pal: usize, grouped_with_zero: &mut HashSet<usize>, adj_list: &Vec<HashSet<usize>>) {
    if grouped_with_zero.insert(pal) {
        // New addition, put all this guy's buddies in the list
        for buddy in adj_list[pal].iter() {
            find_all_rec(*buddy, grouped_with_zero, adj_list);
        }
    }
}

fn main() {
    let adj_list = parse_input();
    let mut grouped_with_zero: HashSet<usize> = HashSet::new();
    find_all_rec(0usize, &mut grouped_with_zero, &adj_list);
    println!("{:?}", grouped_with_zero.len());
}
