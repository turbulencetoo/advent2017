extern crate regex;

use regex::Regex;
use std::collections::HashSet;

fn parse_input() -> Vec<HashSet<usize>> {
    // use vec as an adjacency list where the keys are the indicies
    // 0 <-> 950, 1039
    // 1 <-> 317, 904, 923
    // 2 <-> 2
    let mut out = vec![];
    let re = Regex::new(r"^.* <-> (.*)$").unwrap();
    for line in include_str!("input.txt").lines() {
        out.push(parse_line(&re, line));
    }
    out
}

fn parse_line(re: &Regex, line: &str) -> HashSet<usize> {
    let caps = re.captures(line).unwrap();
    caps.get(1)
        .unwrap()
        .as_str()
        .split(',')
        .map(|s| s.trim()
                  .parse::<usize>())
        .filter_map(|x| x.ok())
        .collect::<HashSet<_>>()
}

fn find_all_rec(pal: usize, group: &mut HashSet<usize>, adj_list: &Vec<HashSet<usize>>) {
    if group.insert(pal) {
        // New addition, put all this guy's buddies in the list
        for buddy in adj_list[pal].iter() {
            find_all_rec(*buddy, group, adj_list);
        }
    }
}

fn main() {
    let mut groups: Vec<HashSet<usize>> = vec![];
    let adj_list = parse_input();
    for i in 0..adj_list.len() {
        if groups.iter().any(|set| set.contains(&i)) {
            continue;
        }
        let mut cur_group: HashSet<usize> = HashSet::new();
        find_all_rec(i, &mut cur_group, &adj_list);
        groups.push(cur_group);
    }
    println!("{:?}", groups.len());
}
