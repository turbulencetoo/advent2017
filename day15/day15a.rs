const A_FACTOR: u64 = 16807;
const B_FACTOR: u64 = 48271;
const DIV: u64 = 2147483647;  // 2^31 - 1
const FORTY_MIL: usize = 40_000_000;

// Generator A starts with 277
// Generator B starts with 349
const A_START: u64 = 277;
const B_START: u64 = 349;

struct Gen {
    cur: u64,
    factor: u64
}

impl Iterator for Gen {
    type Item = u64;
    fn next(&mut self) -> Option<Self::Item> {
        // Never ending iterator
        self.cur = (self.cur * self.factor) % DIV;
        Some(self.cur)
    }
}

fn lowest_sixteen_bits_eq(a: u64, b: u64) -> bool{
    let mask = 0xFFFFu64;
    a & mask == b & mask
}

fn main() {
    let gen_a = Gen {cur: A_START, factor: A_FACTOR};
    let gen_b = Gen {cur: B_START, factor: B_FACTOR};
    let ans = gen_a.zip(gen_b)
                   .take(FORTY_MIL)
                   .filter(|&(a,b)| lowest_sixteen_bits_eq(a,b))
                   .count();
    println!("{:?}", ans);
}