extern crate regex;

use regex::Regex;
use std::collections::{HashMap, HashSet};

#[derive(Debug, Clone)]
struct Node{
    name: String,
    weight: i32,
    // Recursive weight
    rec_weight: i32,
    // This node will own the children it supports
    children: Vec<Node>,

    // Intermediate value to store who the children should be before we look them up
    child_names: Vec<String>
}

impl Node {
    fn new(name: String, weight: i32, children: Vec<Node>, child_names: Vec<String>) -> Self {
        Node {name : name,
              weight : weight,
              rec_weight : 0,
              children : children,
              child_names : child_names}
    }

    fn update_rec_weight(&mut self) -> i32 {
        let rec = self.weight + self.children.iter_mut()
                                             .map(|x| x.update_rec_weight())
                                             .sum::<i32>();
        self.rec_weight = rec;
        rec
    }

    fn find_imbalance_rec(&self) {
        if self.children.len() == 0 {
            return;
        }
        println!("I am {}", self.name);

        let mut weights: HashMap<i32, i32> = HashMap::new();
        for child in self.children.iter() {
            let weight = weights.entry(child.rec_weight).or_insert(0);
            *weight += 1;
        }
        println!("My kids are all {:?}", weights);
        for child in self.children.iter() {
            if weights.get(&child.rec_weight).unwrap() == &1 {
                // Could be a problem child
                println!("{}, {}, ({}) could be a problem",
                         child.name, child.rec_weight, child.weight);
                child.find_imbalance_rec();
            }
        }
        println!("");
    }
}


fn assemble_tree(mut map: HashMap<String, Node>) -> Node {
    let root_name = get_root(&map).clone().name;
    let mut root = map.remove(&root_name).unwrap();
    
    // start recursing
    assemble_tree_rec(&mut root, &mut map);

    // Make sure that after all recursive calls we drained the map that got moved into this func
    assert!(map.is_empty());
    
    root
}

fn assemble_tree_rec(cur: &mut Node, map: &mut HashMap<String, Node>) {
    // Recursive function that takes ownership of all children
    // Do it with no references by moving a node in, and then moving it back out
    for child_name in cur.child_names.iter() {
        let mut child_node = map.remove(child_name).unwrap();
        assemble_tree_rec(&mut child_node, map);
        cur.children.push(child_node);
    }
}

fn get_root(nodes: &HashMap<String, Node>) -> &Node {
    // Operates on a list of nodes that child names, but not children yet
    let all_owned: HashSet<String> = nodes.values()
                                          .flat_map(|n| n.child_names.iter().cloned())
                                          .collect();
    nodes.values()
         .filter(|n| !all_owned.contains(&n.name))
         .next()
         .unwrap()
}

fn parse_input_to_map() -> HashMap<String, Node> {
    // line will look like
    // iceaj (216) -> zbqgiy, taxdaf
    // or
    // zbbdyc (81)
    let re = Regex::new(r"^(.+) \((\d+)\)( -> (.*))?$").unwrap();

    // Map strings to the node with the same name
    let mut out = HashMap::new();
    
    for line in include_str!("input.txt").lines() {
        let new_node = parse_line_to_node(&re, line);
        // Map its name to itself
        out.insert(new_node.name.clone(),
                   new_node);
    }
    out
}

fn parse_line_to_node(re: &Regex, line: &str) -> Node {
    let caps = re.captures(line).unwrap();
    let name = caps.get(1)
                   .unwrap()
                   .as_str()
                   .to_string();
    let weight = caps.get(2)
                     .unwrap()
                     .as_str()
                     .parse::<i32>()
                     .unwrap();
    let mut child_names: Vec<String> = Vec::new();
    if let Some(children_str) = caps.get(4) {
        child_names.extend(children_str.as_str()
                                       .split(',')
                                       .map(|s| s.trim())
                                       .map(|s| s.to_string()));
    }
    Node::new(name, weight, Vec::new(), child_names)
}


fn main() {
    let map = parse_input_to_map();
    let mut root = assemble_tree(map);

    root.update_rec_weight();
    root.find_imbalance_rec();
}
