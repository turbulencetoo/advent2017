extern crate regex;

use regex::Regex;
use std::collections::{HashMap, HashSet};

#[derive(Debug)]
struct Node{
    name: String,
    weight: i32,
    // This node will own the children it supports
    children: Option<Vec<Node>>,

    // Intermediate value to store who the children should be before we look them up
    child_names: Option<Vec<String>>
}

impl Node {
    fn new(name: String, weight: i32,
           children: Option<Vec<Node>>, child_names: Option<Vec<String>>) -> Self {
        Node {name, weight, children, child_names}
    }
}

fn assemble_tree(nodes: &[Node]) -> &Node {
    // Given all the nodes that have their child names populated,
    // we can move those children under parent ownership

    // TODO
    nodes.last().unwrap()
}

fn get_root(nodes: &HashMap<String, Node>) -> &Node {
    // Operates on a list of nodes that child names, but not children yet
    let all_owned: HashSet<String> = nodes.values()
                                          .filter_map(|n| n.child_names.as_ref())
                                          .flat_map(|names| names.iter().cloned())
                                          .collect();
    nodes.values()
         .filter(|n| !all_owned.contains(&n.name))
         .next()
         .unwrap()
}

fn parse_input_to_map() -> HashMap<String, Node> {
    // line will look like
    // iceaj (216) -> zbqgiy, taxdaf
    // or
    // zbbdyc (81)
    let re = Regex::new(r"^(.+) \((\d+)\)( -> (.*))?$").unwrap();

    // Map strings to the node with the same name
    let mut out = HashMap::new();
    
    for line in include_str!("input.txt").lines() {
        let new_node = parse_line_to_node(&re, line);
        // Map its name to itself
        out.insert(new_node.name.clone(),
                   new_node);
    }
    out
}

fn parse_line_to_node(re: &Regex, line: &str) -> Node {
    let caps = re.captures(line).unwrap();
    let name = caps.get(1)
                   .unwrap()
                   .as_str()
                   .to_string();
    let weight = caps.get(2)
                     .unwrap()
                     .as_str()
                     .parse::<i32>()
                     .unwrap();
    let mut child_names: Option<Vec<String>> = None;
    if let Some(children_str) = caps.get(4) {
        child_names = Some(children_str.as_str()
                                       .split(',')
                                       .map(|s| s.trim())
                                       .map(|s| s.to_string())
                                       .collect());
    }
    Node::new(name, weight, None, child_names)
}


fn main() {
    let map = parse_input_to_map();
    let root = get_root(&map);
    println!("{:?}", root);
}
