/*
the perfect squares that are odd make a diagonal that goes down-right from 1
find the next highest perfect square after our input, and that's the box that our input is in
once we have the box, compute the offset from the square in the lower right corner,
once we have the box and the offest, compute the manhattan distance from 1
*/
use std::collections::HashMap;

static INPUT: i32 = 312051;
static UP:    (i32, i32) = ( 0, 1);
static DOWN:  (i32, i32) = ( 0,-1);
static LEFT:  (i32, i32) = (-1, 0);
static RIGHT: (i32, i32) = ( 1, 0);
// need to break a square into triangles that define what square you go to next
//     D L L L L L L
//     D D L L L L U
//     D D D L L U U
//     D D D R U U U
//     D D R R R U U
//     D R R R R R U
//     R R R R R R R
// this shows for a given square what direction we head to get to the next one
// so we need to break a box in to 4 contiguous quadrants
// [last_odd_square+1, corner1) -> up
// [corner1, corner2) -> right
// 
fn get_dir(coords: (i32, i32)) -> (i32,i32) {
    let (x,y) = coords;
    let mut ret = vec![];

    if x == 0 && y == 0 {
        ret.push(RIGHT);
    }
    if x > 0 && x > y.abs() {
        //positive X value and X > |Y| means were in that right quadrant and move up
        ret.push(UP);
    }
    if y > 0 && ((x > 0 && y >= x) ||
                 (x <= 0 && y > x.abs())) {
        //positive Y value and Y >= X for right diagonal or Y > |X| for left diagonal
        ret.push(LEFT);
    }
    if x < 0 && ((y > 0 && x.abs() >= y) ||
                 (y <= 0 && x.abs() > y.abs())) {
        ret.push(DOWN);
    }
    if y < 0 && y.abs() >= x.abs() {
        ret.push(RIGHT);
    }

    // In theory we only executed one of those code blocks, but lets check
    if ret.len() != 1 {
        println!("Got a problem: input is {:?}, out results are {:?}", coords, ret);
        panic!();
    }
    ret.pop().unwrap()
}

fn next(coords: (i32, i32)) -> (i32, i32) {
    let dir = get_dir(coords);
    (coords.0 + dir.0, coords.1 + dir.1)
}

fn sum_all_neighbors (coords: (i32,i32), grid: &HashMap<(i32,i32), i32>) -> i32 {
    let mut total = 0i32;
    for i in &[-1, 0, 1] {
        for j in &[-1, 0, 1] {
            let dir = (*i, *j);
            if dir == (0,0) {
                //this represents us, not a neighbor
                continue;
            }
            let neighbor = (coords.0 + dir.0, coords.1 + dir.1);
            if let Some(&value) = grid.get(&neighbor) {
                total += value;
            }
        }
    }
    total
}

fn main() {
    let mut grid: HashMap<(i32,i32), i32> = HashMap::new();
    let mut cur = (0,0);
    grid.insert(cur, 1);
    loop {
        cur = next(cur);
        let val = sum_all_neighbors(cur, &grid);
        println!("I am {:?} and my neighbors are all {}", cur, val);
        grid.insert(cur, val);
        if val > INPUT {
            println!("The answer is {}", val);
            break;
        }
    }
    // for i in 0..5 {
    //     for j in 0..5 {
    //         let x = i as i32;
    //         let y = j as i32;
    //         let coords = (x,y);
    //         println!("input: {:?}, output: {:?}    add em up: {:?}", coords, get_dir(coords), next(coords));
    //     }
    // }
}
