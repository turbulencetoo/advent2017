/*
the perfect squares that are odd make a diagonal that goes down-right from 1
find the next highest perfect square after our input, and that's the box that our input is in
once we have the box, compute the offset from the square in the lower right corner,
once we have the box and the offest, compute the manhattan distance from 1
*/

static INPUT: u32 = 312051;

static TESTS: [(u32,u32); 3] = [(12,3), (23,2), (1024,31)];

// Return the next highest odd perfect square of a number.
fn next_odd_square(num: u32) -> u32 {
    let mut root = (num as f64).sqrt().ceil();
    if (root as i32) % 2 == 0 {
        // add one to root to make it odd then square it
        root += 1.0f64;
    }
    root.powi(2) as u32
}

// Return the number of cells in the perimeter square that contains our square in the lower right
fn perimeter_size(square: u32) -> u32 {
    side_size(square) * 4
}

// based on the fact that a box with side x is really 4 sides of x-1
fn side_size(square: u32) -> u32 {
    ((square as f64).sqrt() as u32) - 1
}

fn man_distance(num: u32) -> u32 {
    let bottom_right = next_odd_square(num);
    //let start = bottom_right - perimeter_size(bottom_right) + 1; // One square above bottom right
    let side = side_size(bottom_right);
    let corners: [u32; 4] = [bottom_right - side*0,
                             bottom_right - side*1,
                             bottom_right - side*2,
                             bottom_right - side*3];
    //so any corner is ([oddsquareroot] - 1) manhattan moves from center
    // and the further you get from a corner subtracts one from this number (closer to 1) until you are getting close to the other corner

    let mut min_dist = std::i32::MAX;
    // loop to find our distance from the closest corner
    for corner in &corners {
        let dist: i32 = ((num as i32) - (*corner as i32)).abs();
        if dist < min_dist {
            min_dist = dist;
        }
    }

    // based on above logic
    ((bottom_right as f64).sqrt() as u32) - 1 - (min_dist as u32)
}

fn main() {
    for i in 0..123 {
        println!("next square of {} is {}", i, next_odd_square(i as u32));
    }
    for i in &[1,9,25,49,81,121,169] {
        println!("perimeter of {} is {}", i, perimeter_size(*i as u32));
    }

    for test in TESTS.iter() {
        let (test_in, test_out) = *test;
        println!("{} should be {}, i say {}", test_in, test_out, man_distance(test_in));
    }
    println!("{}", man_distance(INPUT));
}
