static INPUT: &'static str = "14  0   15  12  11  11  3   5   1   6   8   4   9   1   8   4";
static TEST: &'static str = "0 2 7 0";

use std::collections::HashMap;
use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};

fn parse_input(input: &str) -> Vec<i32> {
    input.split_whitespace()
         .map(|x| x.parse::<i32>())
         .filter_map(|x| x.ok())
         .collect()
}

fn get_max_idx(list: &[i32]) -> usize {
    list.iter()
        .enumerate()
        .rev()  // Reverse so first is last, because tiebreaker spec'd by puzzle is that first guy gets it
        .max_by_key(|&(_, &num)| num)
        .unwrap()
        .0
}

fn redistribute(list: &mut Vec<i32>) {
    let idx = get_max_idx(list);
    let redist_amt = list[idx];
    list[idx] = 0;

    // mancala
    for i in 1..(redist_amt+1) {
        let mod_idx = (idx + i as usize) % list.len();
        list[mod_idx] += 1;
    }

}

fn hash_vec(list: &[i32]) -> u64 {
    let mut hasher = DefaultHasher::new();
    list.hash(&mut hasher);
    hasher.finish()
}

fn main() {
    let mut input = parse_input(INPUT);
    // let mut input = parse_input(TEST);

    let mut cycles = 0;
    // Map the vector's hash to the index on which it was first seen
    let mut seen: HashMap<u64, i32> = HashMap::new();
    loop {
        if let Some(prev_cycle) = seen.insert(hash_vec(&input), cycles) {
            println!("Saw it on {}, and now also saw it on {} for a diff of {}", prev_cycle, cycles, cycles - prev_cycle);
            break;
        }
        redistribute(&mut input);
        cycles += 1;
    }
}