// With this data structure, the grid is a (y,x) coordinate system with the origin in the upper left
// and x increases to the right, y increases as you go down.
const UP:    (i32, i32) = (-1, 0);
const DOWN:  (i32, i32) = ( 1, 0);
const LEFT:  (i32, i32) = ( 0,-1);
const RIGHT: (i32, i32) = ( 0, 1);

fn is_opposite(dir: (i32, i32), other: (i32, i32)) -> bool {
    dir.0 == -other.0 && dir.1 == -other.1
}

#[derive(Debug)]
struct Map {
    grid: Vec<Vec<char>>,
    dir: (i32, i32),
    cur: (usize, usize),
    collected: Vec<char>,
    counter: u32
}

impl Map {
    fn new(input: Vec<Vec<char>>) -> Self {
        let mut cur = (0,0);
        for (i, ch) in input[0].iter().enumerate() {
            // search the first row for the entry point
            if *ch == '|' {
                cur = (0, i);
                break;
            }
        }
        Map {grid: input, dir: DOWN, cur: cur, collected: Vec::new(), counter: 0u32}
    }

    fn next(&mut self) -> bool {
        // Given the current char and a direction, figure out which way to go
        // return true only if we hit the end
        let cur_ch = self.grid[self.cur.0][self.cur.1];
        match cur_ch {
            '|' => self.keep_going(),
            '-' => self.keep_going(),
            '+' => self.handle_turn(),
            ' ' => return true,
            ch if ch >= 'A' && ch <= 'Z' => {self.collected.push(ch);
                                             self.keep_going();},
            _ => unreachable!()
        }
        false
    }
    fn keep_going(&mut self) {
        self.cur = ((self.cur.0 as i32 + self.dir.0) as usize,
                    (self.cur.1 as i32 + self.dir.1) as usize);
        self.counter += 1;
    }
    fn handle_turn(&mut self) {
        for &dir in [RIGHT, LEFT, UP, DOWN].iter() {
            if is_opposite(dir, self.dir) {
                // dont wanna go back where we came from
                continue;
            }
            let potential_cell = (self.cur.0 as i32 + dir.0,
                                  self.cur.1 as i32 + dir.1);
            if      potential_cell.0 < 0 ||
                    potential_cell.0 >= self.grid.len() as i32 ||
                    potential_cell.1 < 0 ||
                    potential_cell.1 >= self.grid[0].len() as i32 {
                // Don't go off the grid
                continue;
            }
            let cell = self.grid[potential_cell.0 as usize][potential_cell.1 as usize];
            if cell == '|' || cell == '-' {
                // This where we need to go
                self.dir = dir;
                self.keep_going();
                return;
            }
        }
        panic!("Didn't find a suitable direction? {:?}", self);
    }
}

fn parse_input() -> Vec<Vec<char>> {
    include_str!("input.txt")
    .lines()
    .map(|line| line
                .chars()
                .collect::<Vec<_>>())
    .collect::<Vec<_>>()
}

fn main() {
    let mut map = Map::new(parse_input());
    while !map.next() {}
    map.collected.iter().for_each(|ch| print!("{}", ch));
    println!();
    println!("{:?}", map.counter);
}