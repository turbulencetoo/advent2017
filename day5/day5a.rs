
fn parse_input() -> Vec<i32> {
    include_str!("./input.txt").lines()
                               .map(|x| x.parse())
                               .filter_map(|x| x.ok())
                               .collect()
}

fn main() {
    let mut input = parse_input();
    let mut idx: usize = 0;
    let len = input.len();
    let mut ans = 0;
    while idx < len {
        let new_idx = idx as i32 + input[idx];
        input[idx] += 1;
        idx = new_idx as usize;
        ans += 1;
    }
    println!("It took {} steps", ans);
}