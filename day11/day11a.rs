// define a coordinate system
// (shell, side, position)
// https://i.imgur.com/8g6tdMd.png
// shell: how far from the center you are
// side, [0..6) which side you are on, starting from a corner and going clockwise
// position, how far you are from that corner
// position < shell


// Define 
const N: u32 = 0;
const NE: u32 = 1;
const SE: u32 = 2;
const S: u32 = 3;
const SW: u32 = 4;
const NW: u32 = 5;

// then very direction has a corresponding drection for each rotation mod 6
// Same (+0)
// Opposite (+3)

// Out (+1)
// In (+4)

// Clockwise (+2)
// Counterclockwise(+5)


// (shell, side/dir, position)
#[derive(Debug, Copy, Clone)]
enum Coordinate {
    Origin,
    Coord(u32, u32, u32)
}

fn next_coord(current: Coordinate, dir: u32) -> Coordinate {
    use Coordinate::{Origin, Coord};
    // println!("Handling {:?} going {}", current, dir );
    let out = match (current, dir) {
        // If youre at the origin, just go to the first spot for dir
        (Origin, _) => Coord(1, dir, 0),

        // Same means move out 1 shell at same pos and side
        (Coord(shell, side, pos), _) if dir == (side+0) % 6 => Coord(shell+1, side, pos),

        // Opposite means move in 1 shell
        // !!Boundary: if you are near origin you go to origin
        // !!Boundary: if you are at max pos for your shell, you go to shell-1 side+1 pos 0
        (Coord(shell, side, pos), _) if dir == (side+3) % 6 =>  if shell > 1 { 
                                                                    if pos < shell-1 {
                                                                        Coord(shell-1, side, pos)
                                                                    }
                                                                    else {
                                                                        Coord(shell-1, (side+1) % 6, 0u32)
                                                                    }
                                                                }
                                                                else {
                                                                    Origin
                                                                },

        // Out means move out a shell, at same side increasing your position
        (Coord(shell, side, pos), _) if dir == (side+1) % 6 => Coord(shell+1, side, pos+1),

        // In means move in a shell, at same side and decrease position
        // !!Boundary: if you are at position 0 in your sidem,
        // you stay in same shell, go to side -1 (+5) at max position for that side
        // recall that shell-1 is the max position
        (Coord(shell, side, pos), _) if dir == (side+4) % 6 => if pos > 0 {Coord(shell-1, side, pos-1)} else {Coord(shell, (side+5) % 6, shell-1)},

        // Clockwise means same shell, same side, increase position
        // !!Boundary: if you are in max position for your side, then move to side +1 position 0 
        (Coord(shell, side, pos), _) if dir == (side+2) % 6 => if pos < shell-1 {Coord(shell, side, pos+1)} else {Coord(shell, (side+1) % 6, 0u32)},

        // Counterclockwise means same shell, same side, decrease position
        // !!Boundary, if you are at position 0 you go up a shell to side -1 (+5), max position
        // note that ((shell+1)-1) is the max pos for the next shell up
        (Coord(shell, side, pos), _) if dir == (side+5) % 6 => if pos > 0 {Coord(shell, side, pos-1)} else {Coord(shell+1, (side+5) % 6, shell)},
        
        // Above matches should be exhaustive
        (Coord(_, _, _), _) => unreachable!()

    };
    if let Coord(shell, _, pos) = out {
        if pos >= shell {
            panic!("Pos must be strictly less than shell, duh");
        }
    }
    out
}

fn get_input() -> Vec<u32> {
    let mapper = |x: &str| match x {
        "n" => N,
        "ne" => NE,
        "se" => SE,
        "s" => S,
        "sw" => SW,
        "nw" => NW,
        _ => unreachable!()
    };
    include_str!("input.txt").split(',')
                             .map(mapper)
                             .collect()
}

fn main() {
    let mut cur = Coordinate::Origin;
    for dir in get_input() {
        cur = next_coord(cur, dir);
    }
    println!("{:?}", cur);
    // println!("{:?}",next_coord(Coordinate::Origin, N) );
}



// #[derive(Debug, Copy, Clone)]
// enum Dir {
//     N,
//     NE,
//     SE,
//     S,
//     SW,
//     NW
// }
// fn same (dir: Dir) -> Dir {
//     // X + 0
//     dir
// }
// fn opposite(dir: Dir) -> Dir {
//     // X + 3
//     match dir {
//         Dir::N => Dir::S,
//         Dir::NE => Dir::SW,
//         Dir::SE => Dir::NW,
//         Dir::S => Dir::N,
//         Dir::SW => Dir::NE,
//         Dir::NW => Dir::SE
//     }
// }
// fn clockwise(dir: Dir) -> Dir {
//     // X + 2
//     match dir {
//         Dir::N => Dir::SE,
//         Dir::NE => Dir::S,
//         Dir::SE => Dir::SW,
//         Dir::S => Dir::NW,
//         Dir::SW => Dir::N,
//         Dir::NW => Dir::NE
//     }
// }
// fn counter(dir: Dir) -> Dir {
//     // X + 5
//     match dir {
//         Dir::N => Dir::NW,
//         Dir::NE => Dir::N,
//         Dir::SE => Dir::NE,
//         Dir::S => Dir::SE,
//         Dir::SW => Dir::SW,
//         Dir::NW => Dir::SW
//     }
// }
// fn inwards(dir: Dir) -> Dir {
//     // X + 4
//     match dir {
//         Dir::N => Dir::SW,
//         Dir::NE => Dir::NW,
//         Dir::SE => Dir::N,
//         Dir::S => Dir::NE,
//         Dir::SW => Dir::SE,
//         Dir::NW => Dir::S
//     }
// }
// fn outwards(dir: Dir) -> Dir {}
//     // X + 1
//     match dir {
//         Dir::N => Dir::NE,
//         Dir::NE => Dir::SE,
//         Dir::SE => Dir::S,
//         Dir::S => Dir::SW,
//         Dir::SW => Dir::NW,
//         Dir::NW => Dir::N
//     }
// }