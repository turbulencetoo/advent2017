enum Instruction {
    Spin(usize),
    Exchange(usize, usize),
    Partner(char, char)
}

impl Instruction {
    fn from_str(input: &str) -> Self {
        use Instruction::{Spin, Exchange, Partner};
        let (first, rest) = input.split_at(1);
        match first {
            "s" => Spin(rest.parse::<usize>().unwrap()),
            "x" => {let mut swaps = rest.split('/');
                    Exchange(swaps.next().unwrap().parse::<usize>().unwrap(),
                             swaps.next().unwrap().parse::<usize>().unwrap(),)},
            "p" => {let mut swaps = rest.split('/');
                    Partner(swaps.next().unwrap().parse::<char>().unwrap(),
                            swaps.next().unwrap().parse::<char>().unwrap(),)},
            _ => panic!("Panic! at Nabisco")
        }
    }
}

fn spin(progs: Vec<char>, offset: usize) -> Vec<char> {
    let len = progs.len();
    progs.into_iter()
         .cycle()
         .skip(len - offset)
         .take(len)
         .collect()
}

fn exchange(mut progs: Vec<char>, a: usize, b: usize) -> Vec<char> {
    progs.swap(a,b);
    progs
}

fn partner(mut progs: Vec<char>, a: char, b: char) -> Vec<char> {
    let index_a = progs.iter().position(|&x| x == a).unwrap();
    let index_b = progs.iter().position(|&x| x == b).unwrap();
    progs.swap(index_a, index_b);
    progs
}

fn parse_input(input: &str) -> Vec<Instruction> {
    let mut out = vec![];
    for inst in input.split(',') {
        out.push(Instruction::from_str(inst));
    }
    out
}

fn do_all(insts: Vec<Instruction>) -> Vec<char> {
    let mut progs = vec!['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p'];
    for inst in insts {
        progs = match inst {
            Instruction::Spin(offset) => spin(progs, offset),
            Instruction::Exchange(a,b) => exchange(progs, a, b),
            Instruction::Partner(a,b) => partner(progs, a, b)
         }
    }
    progs
}

fn main() {
    let ans = do_all(parse_input(include_str!("input.txt")));
    ans.iter().for_each(|x| print!("{}", x));
    println!("");
}