extern crate regex;

use regex::Regex;
use std::collections::HashMap;

struct Point {
    pos: [i32; 3],
    vel: [i32; 3],
    acc: [i32; 3]
}

impl Point {
    fn update(&mut self) {
        for i in 0..3usize {
            self.vel[i] += self.acc[i];
        }
        for i in 0..3usize {
            self.pos[i] += self.vel[i];
        }
    }
    fn to_pos(&self) -> (i32, i32, i32) {
        (self.pos[0], self.pos[1], self.pos[2])
    }
}

fn parse_input() -> Vec<Point> {
    include_str!("input.txt")
    .lines()
    .map(|l| extract_point(l))
    .collect()
}

fn extract_point(line: &str) -> Point {
    // p=<-2103,773,-994>, v=<-302,112,-140>, a=<25,-6,9>
    let re = Regex::new("p=<(.*)>, v=<(.*)>, a=<(.*)>").unwrap();
    let caps = re.captures(line).unwrap();
    let tuples = (1usize..4usize)
                 .map(|i| caps[i]
                          .split(',')
                          .map(|num| num.parse::<i32>())
                          .collect::<Result<Vec<_>,_>>()
                          .unwrap())
                 .collect::<Vec<_>>();

    Point {pos:[tuples[0][0],
                tuples[0][1],
                tuples[0][2]],
           vel:[tuples[1][0],
                tuples[1][1],
                tuples[1][2]],
           acc:[tuples[2][0],
                tuples[2][1],
                tuples[2][2]]}
}

fn tick(points: &mut Vec<Point>) {
    points.iter_mut()
          .for_each(|point| point.update());
}

fn dedup(points: Vec<Point>) -> Vec<Point> {
    // Add to the output vec any points that are unique.
    let mut counter: HashMap<(i32,i32,i32), u32> = HashMap::new();
    for point in points.iter() {
        *counter.entry(point.to_pos()).or_insert(0) += 1
    }
    points.into_iter()
          .filter(|p| counter.get(&p.to_pos()).unwrap() == &1)
          .collect()
}


fn main() {
    let mut points = parse_input();

    for i in 0..1_000_000 {
        tick(&mut points);
        points = dedup(points);
        if i % 1_000 == 0 {
            println!("{:?}", points.len());
        }
    }
}
