extern crate itertools;

use std::collections::HashMap;
use itertools::Itertools; // Sub-trait of Iterator, now that its in scope I can use any of its methods on an Iterator

#[derive(Debug, Clone)]
struct Scanner {
    len: u32,
}

impl Scanner {
    fn new(_len: u32) -> Self {
        Scanner {len: _len}
    }
    fn will_hit(&self, delay: u32, offset: u32) -> bool {
       (delay + offset) % ((self.len - 1) * 2) == 0
    }
}

struct Firewall {
    layers: Vec<Option<Scanner>>
}

impl Firewall {

    fn new_from_str(input: &str) -> Self {
        let tmp_map: HashMap<u32, u32> = input
            .lines()
            .flat_map(|line| line.split(": ")) // flatten to big list of strings representing numbers
            .filter_map(|n| n.parse::<u32>().ok()) // turn into integers
            .tuples()  // Grab every pair into a tuple
            .collect();

        let max_layer = *tmp_map.keys().max().unwrap() as usize;

        // Allocate a vector to hold all the layers, even the empty ones not in the input
        let mut layers: Vec<Option<Scanner>> = vec![None; max_layer+1];

        // Fill the vector with Scanners in the spots specified by the input
        for (k, v) in tmp_map.iter() {
            layers[*k as usize] = Some(Scanner::new(*v));
        }

        Firewall { layers: layers}
    }

    fn all_safe(&self, delay: u32) -> bool {
        self.layers.iter()
                   .enumerate()
                   .filter(|&(_,x)| x.is_some())
                   .map(|(i,x)| (i, x.as_ref().unwrap()) )
                   .all(|(i,x)| !x.will_hit(delay, i as u32))
    }

}



fn main() {
    let firewall = Firewall::new_from_str(include_str!("input.txt"));
//     let firewall = Firewall::new_from_str(
// "0: 3
// 1: 2
// 4: 4
// 6: 4");
    for i in 0.. {
        if i % 100000 == 0 
        {
            println!("{:?}",i);
        }
        // See if that many ticks is enough
        if firewall.all_safe(i as u32) {
            println!("{:?}", i);
            break;
        }

    }
}