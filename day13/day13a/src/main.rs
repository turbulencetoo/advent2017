extern crate itertools;

use std::collections::HashMap;
use itertools::Itertools; // Sub-trait of Iterator, now that its in scope I can use any of its methods on an Iterator

#[derive(Debug, Clone)]
struct Scanner {
    len: u32,
    pos: u32,
    plus: bool  //Used to determine which direction to advance pos
}

impl Scanner {
    fn new(_len: u32) -> Self {
        Scanner {len: _len, pos: 0u32, plus: true}
    }

    fn tick(&mut self) {
        // Boundary conditions
        if self.pos == 0 {
            self.plus = true;
        }
        else if self.pos == self.len - 1 {
             self.plus = false;
        }

        // Move
        if self.plus {
            self.pos += 1;
        }
        else {
            self.pos -= 1;
        }
    }
}

struct Firewall {
    layers: Vec<Option<Scanner>>,
    cur: usize
}

impl Firewall {

    fn new_from_str(input: &str) -> Self {
        let tmp_map: HashMap<u32, u32> = input
            .lines()
            .flat_map(|line| line.split(": ")) // flatten to big list of strings representing numbers
            .filter_map(|n| n.parse::<u32>().ok()) // turn into integers
            .tuples()  // Grab every pair into a tuple
            .collect();

        let max_layer = *tmp_map.keys().max().unwrap() as usize;

        // Allocate a vector to hold all the layers, even the empty ones not in the input
        let mut layers: Vec<Option<Scanner>> = vec![None; max_layer+1];

        // Fill the vector with Scanners in the spots specified by the input
        for (k, v) in tmp_map.iter() {
            layers[*k as usize] = Some(Scanner::new(*v));
        }

        Firewall { layers: layers, cur: 0}
    }

    fn tick(&mut self) {
        self.layers.iter_mut()
                   .filter_map(|x| x.as_mut())
                   .for_each(|scanner| scanner.tick());
    }
}

impl Iterator for Firewall {
    // Iterator gets the severity at each juncture
    type Item = u32;
    fn next(&mut self) -> Option<Self::Item> {
        if self.cur >= self.layers.len() {
            // End of the road
            return None;
        }
        let mut severity = 0u32;
        // See if we tripped an alarm
        if let Some(ref scanner) = self.layers[self.cur] {
            if scanner.pos == 0 {
                severity = (self.cur as u32) * scanner.len;
            }
        }
        self.cur +=1;
        self.tick();
        Some(severity)
    }
}


fn main() {
    let firewall = Firewall::new_from_str(include_str!("input.txt"));
    println!("{:?}", firewall.sum::<u32>());
}