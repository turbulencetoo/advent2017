extern crate regex;

use regex::Regex;
use std::collections::HashMap;

struct Instruction {
    reg: String,
    val: i32, // Already loaded with the dec/inc parsed into ir
    comp_reg: String,
    comp: String,
    comp_rhs: i32
}

impl Instruction {
    fn execute(&self, registers: &mut HashMap<String, i32>) {
        let comp_lhs: i32 = *registers.entry(self.comp_reg.clone()).or_insert(0);

        let do_it: bool = match self.comp.as_ref() {
            "==" => comp_lhs == self.comp_rhs,
            ">=" => comp_lhs >= self.comp_rhs,
            "<=" => comp_lhs <= self.comp_rhs,
            "!=" => comp_lhs != self.comp_rhs,
            ">" =>  comp_lhs  > self.comp_rhs,
            "<" =>  comp_lhs  < self.comp_rhs,
            _ => unreachable!()
        };
        if do_it {
            *registers.entry(self.reg.clone()).or_insert(0) += self.val;
        }
    }
}

fn parse_line(line: &str, re: &Regex) -> Instruction {
    let caps = re.captures(line).unwrap();
    let reg = caps.get(1)
                 .unwrap()
                 .as_str()
                 .to_string();
    let sign: bool = caps.get(2).unwrap().as_str() == "inc";
    let mut val = caps.get(3)
                      .unwrap()
                      .as_str()
                      .parse::<i32>()
                      .unwrap();
    if !sign {
        val *= -1;
    }
    let comp_reg = caps.get(4)
                       .unwrap()
                       .as_str()
                       .to_string();
    let comp = caps.get(5)
                   .unwrap()
                   .as_str()
                   .to_string();
    let comp_rhs = caps.get(6)
                       .unwrap()
                       .as_str()
                       .parse::<i32>()
                       .unwrap();

    Instruction {reg,
                 val,
                 comp_reg,
                 comp,
                 comp_rhs}

}

fn parse_input() -> Vec<Instruction> {
    let mut out = vec![];

    // lines look like:
    // k dec -567 if wfk == 0
    // jq inc 880 if a < 2
    let re = Regex::new(r"(.+) (inc|dec) (.+) if (.+) (.+) (.+)").unwrap();

    for line in include_str!("input.txt").lines() {
        out.push(parse_line(line, &re));
    }
    out
}

fn main() {
    let mut registers: HashMap<String, i32> = HashMap::new();
    for inst in parse_input() {
        inst.execute(&mut registers);
    }
    println!("{:?}",  registers.values().max().unwrap());
}
