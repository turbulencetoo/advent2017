use std::u8;
const SIZE: usize = u8::MAX as usize + 1;
const HASH_ITER: usize = 64;

// Copied from day 10
fn parse_input(input: &str) -> Vec<usize> {
    let suffix: Vec<usize> = vec![17,31,73,47,23];
    input.bytes()
         .map(|n: u8| n as usize)
         .chain(suffix.into_iter())
         .collect()
}

fn iter_with_mod<'a, T>(list: &'a[T], start: usize, len: usize) -> Box<DoubleEndedIterator<Item = &T> + 'a> {
    let end = start + len;
    if end < list.len() {
        // Every thing is good and we dont need to do any modulo
        Box::new(list[start..end].iter())
    }
    else {
        let mod_end = end % list.len();
        Box::new(list[start..].iter().chain(list[..mod_end].iter()))
    }
}

fn hashy(lengths: Vec<usize>) -> Vec<u8> {
    let mut list = (0..SIZE).map(|i| i as u8).collect::<Vec<_>>();
    let mut pos: usize = 0;
    let mut skip_size: usize = 0;
    for _ in 0..HASH_ITER {
        for length in lengths.clone() {
            // Get the reversed bit of our slice TODO maybe dont clone here, but gotta be smart
            let clone = list.clone();
            let reversed = iter_with_mod(&clone, pos, length).rev();
            // Stick the reversed bit back in our vector
            for (i, item) in reversed.enumerate() {
                let idx = (pos + i) % SIZE;
                list[idx] = *item; // deref to get an i32 which gets copied
            }

            // Set up for next iteration
            pos += length + skip_size;
            pos %= SIZE;
            skip_size += 1;
        }
    }
    list
}

fn sparse_to_dense(sparse: Vec<u8>) -> Vec<u8> {
    assert_eq!(sparse.len(), SIZE);
    let mut dense = vec![];
    for i in 0..16 {
        let mut num = sparse[i*16];
        for j in 1..16 {
            num ^= sparse[i*16 + j];
        }
        dense.push(num);
    }
    dense
}

fn dense_to_hex(dense: Vec<u8>) -> String {
    let mut out = String::new();
    for i in dense {
        out += &format!("{:02x}", i);
    }
    out
}

// end Copied

fn dense_to_bitcount(dense: Vec<u8>) -> u32 {
    assert_eq!(dense.len(), 16usize);
    let sum_byte = |byte: u8| (0..8).map(|shift| 1 << shift)
                                    .map(|bit| if byte & bit == 0 {0u32} else {1u32})
                                    .sum::<u32>();
    dense.into_iter()
         .map(sum_byte)
         .sum()
}

fn string_to_bitcount(input: &str) -> u32 {
    dense_to_bitcount(sparse_to_dense(hashy(parse_input(input))))
}

fn main() {
    let input = "ugkiagan";
    let ans = (0..128).map(|row| string_to_bitcount(&format!("{}-{}", input, row)))
                      .sum::<u32>();
    println!("{:?}", ans);

}