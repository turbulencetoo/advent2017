use std::u8;
use std::collections::HashSet;
const SIZE: usize = u8::MAX as usize + 1;
const HASH_ITER: usize = 64;
const BITMAP_LEN: i32 = 128;

// Copied from day 10
fn parse_input(input: &str) -> Vec<usize> {
    let suffix: Vec<usize> = vec![17,31,73,47,23];
    input.bytes()
         .map(|n: u8| n as usize)
         .chain(suffix.into_iter())
         .collect()
}

fn iter_with_mod<'a, T>(list: &'a[T], start: usize, len: usize) -> Box<DoubleEndedIterator<Item = &T> + 'a> {
    let end = start + len;
    if end < list.len() {
        // Every thing is good and we dont need to do any modulo
        Box::new(list[start..end].iter())
    }
    else {
        let mod_end = end % list.len();
        Box::new(list[start..].iter().chain(list[..mod_end].iter()))
    }
}

fn hashy(lengths: Vec<usize>) -> Vec<u8> {
    let mut list = (0..SIZE).map(|i| i as u8).collect::<Vec<_>>();
    let mut pos: usize = 0;
    let mut skip_size: usize = 0;
    for _ in 0..HASH_ITER {
        for length in lengths.clone() {
            // Get the reversed bit of our slice TODO maybe dont clone here, but gotta be smart
            let clone = list.clone();
            let reversed = iter_with_mod(&clone, pos, length).rev();
            // Stick the reversed bit back in our vector
            for (i, item) in reversed.enumerate() {
                let idx = (pos + i) % SIZE;
                list[idx] = *item; // deref to get an i32 which gets copied
            }

            // Set up for next iteration
            pos += length + skip_size;
            pos %= SIZE;
            skip_size += 1;
        }
    }
    list
}

fn sparse_to_dense(sparse: Vec<u8>) -> Vec<u8> {
    assert_eq!(sparse.len(), SIZE);
    let mut dense = vec![];
    for i in 0..16 {
        let mut num = sparse[i*16];
        for j in 1..16 {
            num ^= sparse[i*16 + j];
        }
        dense.push(num);
    }
    dense
}
// end Copied


fn string_to_bitmap(input: &str) -> HashSet<(i32,i32)> {
    // Return a set containing all the coordinates that are on

    // func that takes a byte and returns an iterator of 8 bools
    let to_bits = |byte: u8| (0..8).rev() // start with MSB and go to LSB
                                   .map(|shift| 1 << shift)
                                   .map(|bit| !(byte & bit == 0))
                                   .collect::<Vec<bool>>();

    let hash = |row, input| sparse_to_dense(hashy(parse_input(&format!("{}-{}", input, row))));

    let row_idx_to_tuples = |row| hash(row, input)
                                  .into_iter()
                                  .flat_map(|byte| to_bits(byte).into_iter()) // now we have 128 bools
                                  .enumerate()       // get all indices
                                  .filter(|&(_,x)| x) // take only the true ones
                                  .map(|(column, _)| (row as i32, column as i32))
                                  .collect::<Vec<(i32,i32)>>();

    (0..BITMAP_LEN).flat_map(|row| row_idx_to_tuples(row).into_iter())
                   .collect::<HashSet<_>>()
}

fn prune_group_rec(guy: (i32, i32), bitmap: &mut HashSet<(i32,i32)>) {
    // recursively remove an entire adjacent blob from the bitmap
    if !bitmap.remove(&guy) {
        return
    }
    [(1,0), (0,1), (-1,0), (0,-1)].iter()
                                  .map(|&(x,y)| (guy.0+x, guy.1+y))
                                  .for_each(|neighbor| prune_group_rec(neighbor, bitmap))
}

fn count_groups(bitmap: &mut HashSet<(i32,i32)>) -> u32 {
    let mut count = 0u32;
    for i in 0..BITMAP_LEN {
        for j in 0..BITMAP_LEN {
            if bitmap.contains(&(i,j)) {
                count += 1;
                prune_group_rec((i,j), bitmap);
            }
        }
    }
    count
}

fn main() {
    let input = "ugkiagan";
    // let input = "flqrgnkx";
    let mut map = string_to_bitmap(input);
    for row in 0..8 {
        for col in 0..8 {
            print!("{}", if map.contains(&(row,col)) {"X"} else {"."});
        }
        println!("");
    }
    println!("total true {:?}", map.len());
    let ans  = count_groups(&mut map);
    println!("{:?}", ans);

}