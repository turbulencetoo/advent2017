enum ParseMode {
    Group,
    Garbage
}

fn calculate_score(input: &str) -> i32 {
    let mut input_iter = input.chars();

    // Set up initial conditions
    let mut mode = ParseMode::Group;
    let mut depth = 1; // What is each group worth at this level
    let mut total = 0;
    let mut skip_next = false;

    // Take the first bracket off the front
    assert_eq!(input_iter.next(), Some('{'));

    for ch in input_iter {
        // println!("{:?}", ch);
        if skip_next {
            skip_next = false;
            continue;
        }
        match mode {

            ParseMode::Group => match ch {
                '{' => depth += 1,
                '}' => {total += depth;
                        depth -= 1;
                        },
                '<' => mode = ParseMode::Garbage,
                _ => ()
                },

            ParseMode:: Garbage => match ch {
                '!' => skip_next = true,
                '>' => mode = ParseMode::Group,
                  _ => ()
                }

        }
    }

    total
}


fn main() {
    for example in include_str!("examples.txt").lines() {
        println!("{:?} Score of {}", example, calculate_score(example));
    }
    let ans = calculate_score(include_str!("input.txt"));
    println!("{:?}", ans);
}