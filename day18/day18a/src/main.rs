extern crate num;
use num::bigint::BigInt;
use num::Zero;
use num::traits::ToPrimitive;
use std::collections::HashMap;

// Enum that indicates whether an operand is a register or a literal value
#[derive(Debug, Clone)]
enum Value {
    Reg(String),
    Num(i32),
    Na, // For when there isnt a second instruction
}

impl Value {
    fn from_str(input: &str) -> Self {
        if let Ok(num) = input.parse::<i32>() {
            Value::Num(num)
        }
        else {
            Value::Reg(input.to_owned())
        }
    }
}


// Struct that represents one line of the program
#[derive(Debug, Clone)]
struct Instruction {
    cmd: String,
    X: Value,
    Y: Value
}

impl Instruction {
    fn new(_cmd: &str, _x: Value, _y: Value) -> Self {
        Instruction{ cmd: _cmd.to_owned(), X: _x, Y: _y }
    }
}

 
// The whole program space and register space + an instruction pointer
struct Duet {
    insts: Vec<Instruction>,
    registers: HashMap<String, BigInt>,
    ip: i32,
    sound: BigInt
}

impl Duet {
    fn extract_value(&mut self, val: &Value) -> BigInt {
        match val {
            &Value::Reg(ref key) => self.registers.entry(key.clone()).or_insert(BigInt::zero()).clone(),
            &Value::Num(ref num) => BigInt::from(*num),
            &Value::Na => panic!("Dont extract from NA!")
        }
    }
    fn execute_one(&mut self) {
        let cur_inst = self.insts[self.ip as usize].clone(); // AAAA
        match cur_inst.cmd.as_ref() {
            "snd" => self.sound = self.extract_value(&cur_inst.X),
            "set" => if let Value::Reg(key) = cur_inst.X {
                         let set_val = self.extract_value(&cur_inst.Y);
                         self.registers.insert(key.clone(), set_val);
                     },
            "add" => if let Value::Reg(key) = cur_inst.X {
                         let rhs = self.extract_value(&cur_inst.Y);
                         let entry = self.registers.entry(key.clone()).or_insert(BigInt::zero());
                         *entry = entry.clone() + rhs;
                     },
            "mul" => if let Value::Reg(key) = cur_inst.X {
                         let rhs = self.extract_value(&cur_inst.Y);
                         let entry = self.registers.entry(key.clone()).or_insert(BigInt::zero());
                         *entry = entry.clone() * rhs;
                     },
            "mod" => if let Value::Reg(key) = cur_inst.X {
                         let rhs = self.extract_value(&cur_inst.Y);
                         let entry = self.registers.entry(key.clone()).or_insert(BigInt::zero());
                         *entry = entry.clone() % rhs;
                     },
            "rcv" => if self.extract_value(&cur_inst.X) > BigInt::zero() {
                        println!("RECOVERING {:?}", self.sound);
                        panic!("done here!");
                     },
            "jgz" => if self.extract_value(&cur_inst.X) > BigInt::zero() {
                        self.ip += self.extract_value(&cur_inst.Y).to_i32().unwrap();
                        self.ip -= 1; // To supress the fact that we're adding one at the end
                     },
            _ => unreachable!("bwaak")
        }
        self.ip += 1;
    }
}

fn parse_input() -> Vec<Instruction> {
    // snd b
    // add i -1
    let mut out = vec![];
    for line in include_str!("input.txt").lines(){
        let tokens = line.split(' ').collect::<Vec<_>>();
        let x = Value::from_str(tokens[1]);
        let y;
        if tokens.len() == 3 {
            y = Value::from_str(tokens[2]);
        }
        else {
            y = Value::Na;
        }
        out.push(Instruction::new(tokens[0], x, y));
    }
    out
}

fn main() {
    let mut duet = Duet{insts: parse_input(),
                        registers: HashMap::new(),
                        ip: 0,
                        sound: BigInt::zero()};
    loop {
        duet.execute_one();
    }
}