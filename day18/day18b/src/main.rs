extern crate num;
use num::bigint::BigInt;
use num::Zero;
use num::traits::ToPrimitive;
use std::collections::{HashMap, VecDeque};

// Enum that indicates whether an operand is a register or a literal value
#[derive(Debug, Clone)]
enum Value {
    Reg(String),
    Num(i32),
    Na, // For when there isnt a second instruction
}

impl Value {
    fn from_str(input: &str) -> Self {
        if let Ok(num) = input.parse::<i32>() {
            Value::Num(num)
        }
        else {
            Value::Reg(input.to_owned())
        }
    }
}


// Struct that represents one line of the program
#[derive(Debug, Clone)]
struct Instruction {
    cmd: String,
    X: Value,
    Y: Value
}

impl Instruction {
    fn new(_cmd: &str, _x: Value, _y: Value) -> Self {
        Instruction{ cmd: _cmd.to_owned(), X: _x, Y: _y }
    }
}

 
// The whole program space and register space + an instruction pointer
struct Program {
    insts: Vec<Instruction>,
    registers: HashMap<String, BigInt>,
    ip: i32
}


impl Program {

    fn new(_insts: &Vec<Instruction>, id: usize) -> Self {
        let mut _registers: HashMap<String, BigInt> = HashMap::new();
        _registers.insert("p".to_owned(), BigInt::from(id));

        Program {insts: _insts.clone(),
                 registers: _registers,
                 ip: 0}
    }

    fn extract_value(&mut self, val: &Value) -> BigInt {
        match val {
            &Value::Reg(ref key) => self.registers.entry(key.clone()).or_insert(BigInt::zero()).clone(),
            &Value::Num(ref num) => BigInt::from(*num),
            &Value::Na => panic!("Dont extract from NA!")
        }
    }
    fn execute_one(&mut self, my_queue: &mut VecDeque<BigInt>, target_queue: &mut VecDeque<BigInt>) -> bool {
        // does one instruction, returns true if it can continue, false if it is waiting for something on the queue
        let cur_inst = self.insts[self.ip as usize].clone(); // AAAA
        match cur_inst.cmd.as_ref() {
            "snd" => target_queue.push_back(self.extract_value(&cur_inst.X)),
            "set" => if let Value::Reg(key) = cur_inst.X {
                         let set_val = self.extract_value(&cur_inst.Y);
                         self.registers.insert(key.clone(), set_val);
                     },
            "add" => if let Value::Reg(key) = cur_inst.X {
                         let rhs = self.extract_value(&cur_inst.Y);
                         let entry = self.registers.entry(key.clone()).or_insert(BigInt::zero());
                         *entry = entry.clone() + rhs;
                     },
            "mul" => if let Value::Reg(key) = cur_inst.X {
                         let rhs = self.extract_value(&cur_inst.Y);
                         let entry = self.registers.entry(key.clone()).or_insert(BigInt::zero());
                         *entry = entry.clone() * rhs;
                     },
            "mod" => if let Value::Reg(key) = cur_inst.X {
                         let rhs = self.extract_value(&cur_inst.Y);
                         let entry = self.registers.entry(key.clone()).or_insert(BigInt::zero());
                         *entry = entry.clone() % rhs;
                     },
            "rcv" => {  if let Some(sound) = my_queue.pop_front() {
                            if let Value::Reg(key) = cur_inst.X {
                                self.registers.insert(key.clone(), sound);
                            }
                            else {
                                panic!("Tried to recv into non string register?");
                            }
                        }
                        else {
                            // Wait for stuff on the queue
                            return false;
                        }
                     },
            "jgz" => if self.extract_value(&cur_inst.X) > BigInt::zero() {
                        self.ip += self.extract_value(&cur_inst.Y).to_i32().unwrap();
                        self.ip -= 1; // To supress the fact that we're adding one at the end
                     },
            _ => unreachable!("bwaak")
        }
        self.ip += 1;
        true
    }
}

struct Duet {
    programs: [Program; 2],
    queue_zero: VecDeque<BigInt>,
    queue_one: VecDeque<BigInt>,
}
impl Duet {
    fn perform(&mut self) {
        // Runs both programs forever. When it deadlocks, user will see how often a value was sent
        let mut counter = 0u32;
        loop {
            for i in 0usize..2usize {
                let (my_queue, their_queue) = if i == 0usize { (&mut self.queue_zero, &mut self.queue_one) }
                                                        else { (&mut self.queue_one, &mut self.queue_zero) };

                loop {
                    let cur_queue_size = their_queue.len();
                    if !self.programs[i].execute_one(my_queue, their_queue) {
                        break;
                    }
                    if i == 1usize && their_queue.len() > cur_queue_size {
                        // This is what we count for the answer to the problem
                        counter += 1
                    }
                }
                println!("{} halted, counter is {}", i, counter);
            }
        }
    }
}

fn parse_input() -> Vec<Instruction> {
    // snd b
    // add i -1
    let mut out = vec![];
    for line in include_str!("input.txt").lines(){
        let tokens = line.split(' ').collect::<Vec<_>>();
        let x = Value::from_str(tokens[1]);
        let y;
        if tokens.len() == 3 {
            y = Value::from_str(tokens[2]);
        }
        else {
            y = Value::Na;
        }
        out.push(Instruction::new(tokens[0], x, y));
    }
    out
}

fn main() {
    let input = parse_input();
    let mut duet = Duet{ programs: [Program::new(&input, 0usize), Program::new(&input, 1usize)],
                         queue_zero: VecDeque::new(),
                         queue_one: VecDeque::new()};
    duet.perform();
}