const STEPS: usize = 386;

fn pretty_print(values: &[i32], cur: usize) {
    for (i, num) in values.iter().enumerate() {
        if i == cur {
            print!("({})", num);
        }
        else {
            print!("{}", num);
        }
        print!(" ");
    }
    println!("");
}

fn main() {
    let mut values: Vec<i32> = vec![];
    values.push(0);
    let mut cur = 0usize;

    for i in 1..2018i32 {
        cur = (cur + STEPS) % values.len();
        values.insert(cur+1, i);
        cur = (cur + 1) % values.len();
        //pretty_print(&values, cur)
    }
    cur = (cur + 1) % values.len();
    println!("{}", values[cur]);
}