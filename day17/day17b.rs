const STEPS: usize = 386;

fn main() {
    // We only car about inserts that happen at index 1, as 0 is always at index 0
    // so just keep track of the length of the vector
    let mut len: usize = 1; // Vector's length
    let mut ans = 0i32; // Most recent item inserted at 1
    let mut cur = 0usize; // Current spot

    for i in 1..50_000_001i32 {
        cur = (cur + STEPS) % len;
        // See if we're inserting after 0
        if cur == 0 {
            ans = i;
        }
        // Do the 'insert'
        len += 1;
        // move cur by 1
        cur = (cur + 1) % len;
    }
    println!("{}", ans);
}