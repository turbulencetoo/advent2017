static INPUT: &'static str = "14,58,0,116,179,16,1,104,2,254,167,86,255,55,122,244";
const SIZE: usize = 256;

fn parse_input(input: &str) -> Vec<usize> {
    input.split(',')
         .map(|s| s.parse::<usize>())
         .filter_map(|r| r.ok())
         .collect()
}

fn iter_with_mod<'a, T>(list: &'a[T], start: usize, len: usize) -> Box<DoubleEndedIterator<Item = &T> + 'a> {
    let end = start + len;
    if end < list.len() {
        // Every thing is good and we dont need to do any modulo
        Box::new(list[start..end].iter())
    }
    else {
        let mod_end = end % list.len();
        Box::new(list[start..list.len()].iter().chain(list[0..mod_end].iter()))
    }
}

fn hashy(lengths: Vec<usize>) -> Vec<i32> {
    let mut list = (0..SIZE).map(|i| i as i32).collect::<Vec<_>>();
    let mut pos: usize = 0;
    let mut skip_size: usize = 0;
    for length in lengths {
        // Get the reversed bit of our slice TODO maybe dont clone here, but gotta be smart
        let clone = list.clone();
        let reversed = iter_with_mod(&clone, pos, length).rev();
        // Stick the reversed bit back in our vector
        for (i, item) in reversed.enumerate() {
            let idx = (pos + i) % SIZE;
            list[idx] = *item; // deref to get an i32 which gets copied
        }

        // Set up for next iteration
        pos += length + skip_size;
        pos %= SIZE;
        skip_size += 1;
    }
    list
}

fn main() {
    let out = hashy(parse_input(INPUT));
    let ans = out[0] * out[1];
    println!("{:?}", ans);
}